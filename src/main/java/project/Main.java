package project;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class Main {
    private static String FILE = "f:/zasoby/maven_project/resume.pdf";
    private static  int CELL_NUM = 10;
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);

    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(FILE));
        document.open();
        addTitle(document);
        createTable(document);
        document.close();
    }

    private static void addTitle(Document document)
            throws DocumentException {
        Paragraph paragraph = new Paragraph("Resume", catFont);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(paragraph, 1);
        addEmptyLine(paragraph, 1);
        document.add(paragraph);
    }
    private static void createTable(Document document)
            throws DocumentException {
        PdfPTable table = new PdfPTable(2);

        String[] contents = {"First name","Artur","Last name","Hamernik","Profession","Student","Education","2018 - 2021 Państwowa Wyższa Szkoła Zawodowa","Summary","I'm a great dude!"};
        for(int i = 0; i < CELL_NUM; i++){
            PdfPCell c1 = new PdfPCell(new Phrase(contents[i]));
            c1.setPadding(5);
            table.addCell(c1);
        }

        document.add(table);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

}
